'use strict';

document.addEventListener('DOMContentLoaded', function () {
    // бутерброд
    var mobileBar = document.querySelector('.mobile-bar');
    var navBar = document.querySelector('.navbar');

    var openMobileBar = () => {
        mobileBar.classList.remove('mobile-bar_hidden');
        mobileBar.style.display = 'block';

        navBar.classList.add('navbar_hidden');
    };
    var closeMobileBar = () => {
        mobileBar.classList.add('mobile-bar_hidden');

        setTimeout(() => {
            mobileBar.style.display = 'none';
        }, 495);
        navBar.classList.remove('navbar_hidden');
    };

    document.querySelector('.navbar__burger').addEventListener('click', openMobileBar);

    document.querySelector('.mobile-bar__close').addEventListener('click', closeMobileBar);
    document.querySelectorAll('.mobile-bar__menu .menu__link').forEach(link => link.addEventListener('click', closeMobileBar));

    // белый фон на менюшке после прокрутки
    var scrollHandler = () => {
        var navbarClassList = document.querySelector('.navbar').classList;
        var burger = document.querySelector('.navbar__burger').classList;

        if (window.pageYOffset < 50) {
            navbarClassList.remove('navbar_white-bg');
            burger.remove('burger_black');
        } else {
            navbarClassList.add('navbar_white-bg');
            burger.add('burger_black');
        }
    };

    scrollHandler();

    window.addEventListener('scroll', scrollHandler);

    // слайдеры
    Swiper ('.reviews__slider', {
        loop: true,
        grabCursor: true,
        pagination: '.swiper-pagination'
    });

    var employeesSlider = new Swiper ('.employees__slider', {
        loop: true,
        grabCursor: true,
        slidesPerView: 3,
        pagination: '.swiper-pagination'
    });

    var slidesResize = () => {
        const width = window.innerWidth;

        if (width >= 1110) {
            employeesSlider.params.slidesPerView = 3;
            employeesSlider.update();
        }

        if (width < 1110) {
            employeesSlider.params.slidesPerView = 2;
            employeesSlider.update();
        }

        if (width < 740) {
            employeesSlider.params.slidesPerView = 1;
            employeesSlider.update();
        }
    };

    slidesResize();

    window.addEventListener('resize', slidesResize);

    // плавный переход по анкорам 
    (function () {
        'use strict';
        // Feature Test
        if ('querySelector' in document && 'addEventListener' in window && Array.prototype.forEach) {
            // Function to animate the scroll
            var smoothScroll = function (anchor, duration, dataID) {
                // Calculate how far and how fast to scroll
                var startLocation = window.pageYOffset;
                var endLocation = anchor.offsetTop;
                var distance = endLocation - startLocation;
                var increments = distance / (duration / 16);
                var stopAnimation;

                // Scroll the page by an increment, and check if it's time to stop
                var animateScroll = function () {
                    window.scrollBy(0, increments);
                    stopAnimation();
                };

                // Loop the animation function
                var runAnimation = setInterval(animateScroll, 16);

                // If scrolling down
                if (increments >= 0) {
                    // Stop animation when you reach the anchor OR the bottom of the page
                    stopAnimation = function () {
                        var travelled = window.pageYOffset;
                        if ((travelled >= (endLocation - increments)) || ((window.innerHeight + travelled) >= (document.body.offsetHeight - 10))) {
                            clearInterval(runAnimation);
                            location.hash = dataID;
                        }
                    };
                } else {
                    // If scrolling up
                    // Stop animation when you reach the anchor OR the top of the page
                    stopAnimation = function () {
                        var travelled = window.pageYOffset;
                        if (travelled <= (endLocation || 0)) {
                            clearInterval(runAnimation);
                            location.hash = dataID;
                        }
                    };
                }
            };

            // Define smooth scroll links
            var scrollToggle = document.querySelectorAll('.scroll');

            // For each smooth scroll link
            [].forEach.call(scrollToggle, function (toggle) {
                // When the smooth scroll link is clicked
                toggle.addEventListener('click', function (e) {
                    // Prevent the default link behavior
                    e.preventDefault();

                    // Get anchor link and calculate distance from the top
                    var dataID = toggle.getAttribute('href');
                    var dataTarget = document.querySelector(dataID);
                    var dataSpeed = toggle.getAttribute('data-speed');

                    // If the anchor exists
                    if (dataTarget) {
                        // Scroll to the anchor
                        smoothScroll(dataTarget, dataSpeed || 500, dataID);
                    }
                }, false);
            });
        }
    })();
});
